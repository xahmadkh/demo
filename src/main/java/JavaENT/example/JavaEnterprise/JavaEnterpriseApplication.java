package JavaENT.example.JavaEnterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class JavaEnterpriseApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaEnterpriseApplication.class, args);
        List<String> messages = new ArrayList<>();
        messages.add("Hello");
        messages.add("World 2");
        System.out.println(messages);

    }

}
